import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { Project } from "../../models/project";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})

export class ProjectsComponent implements OnInit {
  projects: Project[] = [];
  currentProject: Project = null;
  currentIndex: number = 1;
  barStyle: any = {}

  constructor(public data: DataService) {
  }

  ngOnInit() {
    this.projects = this.data.projects;
    this.currentProject = this.data.projects[0];
    this.barStyle.width = `${1 / this.projects.length * 100}%`;
  }

  onProjectChange(e) {
    if (e < this.projects.length) {
      this.barStyle.width = `${(e + 1) / this.projects.length * 100}%`;
      var currentProjectId = this.data.projects[e].id;
      this.data.setCurrentProject(currentProjectId)
      this.currentIndex = e + 1;    
    }

  }
}
