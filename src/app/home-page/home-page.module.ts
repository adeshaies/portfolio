import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { ContactComponent } from './contact/contact.component';
import { AppRoutingModule } from '../app-routing.module';
import { DragScrollModule } from 'ngx-drag-scroll';



@NgModule({
  declarations: [HomePageComponent, AboutComponent, ProjectsComponent, ContactComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    DragScrollModule
  ],
  exports: [
    HomePageComponent
  ]
})
export class HomePageModule { }
