import { Injectable } from '@angular/core';
import siteData from '../assets/data.json';
import { Project } from "./models/project";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  langue = "Francais";
  texts = siteData["fr"];
  projects: Project[] = siteData['fr']["projects"];
  currentProject: Project = this.projects[0];

  constructor() {

  }

  setCurrentProject(id) {
    this.currentProject = this.projects.filter((proj) => {
      return proj.id === id;
    })[0];
  }

  modifierLangue() {
    if (this.langue == "Francais") {
      this.langue = "English";
      localStorage.setItem("langue", "eng");
      this.texts = siteData['eng'];
      this.projects = siteData['eng']['projects'];
      this.setCurrentProject(this.currentProject.id)
    } else {
      this.langue = "Francais";
      localStorage.setItem("langue", "fr");
      this.texts = siteData['fr'];
      this.projects = siteData['fr']['projects'];
      this.setCurrentProject(this.currentProject.id)
    }
  }
}
