import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { HomePageComponent } from './home-page/home-page/home-page.component';
import { AboutComponent } from './home-page/about/about.component';
import { ContactComponent } from './home-page/contact/contact.component';
import { ProjectsComponent } from './home-page/projects/projects.component';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';


const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'detail/:id', component: ProjectDetailsComponent },
  { path: '', component: HomePageComponent },
  { path: '**', component: HomePageComponent }

];

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
