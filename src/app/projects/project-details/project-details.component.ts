import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { Project } from "../../models/project";
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  id: number;
  position: number;
  project: Project;
  allCategories: String;
  arrCategories: any;
  dataService: DataService

  constructor(protected data: DataService, protected route: ActivatedRoute) {
    this.dataService = data

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']
    })
    this.dataService.setCurrentProject(this.id)

    this.position = this.dataService.texts.projects.indexOf(this.data.currentProject);
  }

  verifyDesc() {
    if (this.data.currentProject.subTitle2 == "") {
      return false;
    }
    else {
      return true;
    }
  }

  verifyVideo() {
    if (this.id != 1) {
      return false;
    }
    else {
      return true;
    }
  }
}
