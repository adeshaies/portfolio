export interface Project {
    id: String,
    shortTitle: String,
    title: String,
    year: String,
    type: String,
    shortDesc: String,
    category: String,
    subTitle1: String,
    subText1: String,
    subTitle2: String,
    subText2: String,
    urlImage: String
}
