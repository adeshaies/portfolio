import { Component, HostListener, ElementRef, ViewChild } from '@angular/core';
import { DataService } from './data.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  title = 'portfolio';
  top: any;
  left: any;
  expand = false;

  //Menu
  stateMenu: string = "nav";
  bgMenu: string = "menu__background";
  menuItems: any[] = [];
  closestMenuItemId: any = null;
  stateMenuItem: string = "nav__list";

  constructor(public data: DataService) {
  }

  ngOnInit() {

    //Menu
    $(window).scroll(() => {
      var scroll = $(window).scrollTop();

      if (scroll < 150) {
        this.stateMenu = "nav";
        this.stateMenuItem = "nav__list"
        this.bgMenu = "menu__background";
      } else {
        this.stateMenu = "nav minimized";
        this.bgMenu = "menu__background menu__background--white";

        var menuItems = [
          $("#projects"),
          $("#about"),
          $("#contact")
        ]

        menuItems = menuItems.filter((e) => { return e.length })

        menuItems.forEach((item) => {
          item.distance = Math.abs(scroll - item.position().top);
        })
        var closestMenuItem = menuItems.reduce((min, p) => p.distance < min.distance ? p : min, menuItems[0]);

        if (closestMenuItem !== undefined) {
          this.closestMenuItemId = closestMenuItem.attr('id');
          this.stateMenuItem = "nav__list nav__list--" + this.closestMenuItemId;
        }
      }
    });


  }
  modifierLangue() {
    this.data.modifierLangue()
  }

  setPosition(element, e, velocity) {
    var x, y

    if (velocity > 0) {
      var angle = Math.atan2(e.clientY - element.position().top, e.clientX - element.position().left);
      var xVelocity = velocity * Math.cos(angle);
      var yVelocity = velocity * Math.sin(angle);
      x = element.position().left + xVelocity
      y = element.position().top + yVelocity

    } else {
      x = e.clientX
      y = e.clientY
    }

    $(element).css("transform", `translate3d(${x}px, ${y}px, 0)`)
  }

  //Custom Mouse

  @HostListener('document:click', ['$event'])
  onclick($event) {
    this.expand = true;
    setTimeout(() => {
      this.expand = false;
    }, 500)
  }

  @HostListener('document:mousemove', ['$event'])
  onmousemouve($event) {
    this.top = ($event.pageY - 10) + "px";
    this.left = ($event.pageX - 10) + "px";
  }


}

